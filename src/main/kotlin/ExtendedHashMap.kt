class ExtendedHashMapException(message:String): Exception(message)


class ExtendedHashMap : HashMap<String, Int>() {

    val iloc = ILOC()
    val ploc = PLOC()


    fun iloc(i: Int): Int? {
        /**
         * function receive hashmap index
         *
         * Returns value of key with index i
         *
         */
        val ss = this.keys.toSortedSet()
        return if (i < 0 || i >= ss.size) {
            null
        } else {
            this[ss.elementAt(i)]
        }
    }

    fun ploc(cond: String): HashMap<String, Int> {
        /**
         * function receive condition string. Eg: (>10, <20, <>30)
         *
         * Returns ExtendedHashMap of valid keys
         *
         */
        val conditions: List<String> = cond.split(",").map { it -> it.trim() }
        val result = ExtendedHashMap()

        this.keys.forEach{ key ->
            val subKeys = key.trim('(', ')').split(",").map { it -> it.trim() }
            if (subKeys.size == conditions.size) {
                var flag = true;
                for (i in subKeys.indices) {

                    if (conditions[i].length < 2 || !evalCondition(subKeys[i], conditions[i])) {
                        flag = false
                    }
                }
                if (flag) {
                    result[key] = this[key]?:0
                }
            }
        }
        return result
    }

    private fun evalCondition(key: String, condition: String): Boolean{
        /**
         * function receive two Strings: "Number", "Condition Number". Eg: evalCondition("1", "<=5")
         * Input String of condition and number. eg ">=1056"
         * Returns true if expression is true
         * Allowed condition operators: >, >=, <, <=, =, <>
         */
        val opRight = parseCondition(condition)
        val op = opRight.first
        var right = 0
        var left = 0
        try {
            left = key.toInt()
            right = opRight.second.toInt()
        } catch (e: Exception) {
            return false
        }

        return when (op) {
            ">" -> left > right
            ">=" -> left >= right
            "<" -> left < right
            "<=" -> left <= right
            "=" -> left == right
            "<>" -> left != right
            else -> throw ExtendedHashMapException("wrong operator. Only '>', '>=', '<', '<=', '=', '<>' are available") // unreachable code
        }
    }

    private fun parseCondition(condition: String): Pair<String, String> {
        /**
         * Split string into condition and number
         * Input String of condition and number. eg ">=1056"
         * Output Pair(String, String). Pair(">=", "1056")
         * Allowed condition operators: >, >=, <, <=, =, <>
         */

        condition.trim(' ')
            val firstChar = condition[0]
            val secondChar = condition[1]

            val op = when (firstChar) {
                '=' -> "="
                '>' -> when (secondChar) {
                    '=' -> ">="
                    else -> ">"
                }
                '<' -> when (secondChar) {
                    '=' -> "<="
                    '>' -> "<>"
                    else -> "<"
                }
                else -> "" // unreachable code
            }

            return when (op.length) {
                1 -> Pair(op, condition.substring(1).trim(' '))
                2 -> Pair(op, condition.substring(2).trim(' '))
                else -> Pair("", "") // unreachable code
            }
    }

    inner class ILOC(){
        operator fun get(index: Int): Int? {
            return this@ExtendedHashMap.iloc(index)
        }
    }

    inner class PLOC(){
        operator fun get(cond: String): HashMap<String, Int> {
            return this@ExtendedHashMap.ploc(cond)
        }
    }
}

